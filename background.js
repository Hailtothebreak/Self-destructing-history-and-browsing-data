
var appcache;
var cache;
var cookies;
var downloads;
var fileSystems;
var formData;
var chistory;
var cindexedDB;
var clocalStorage;
var serverBoundCertificates;
var pluginData;
var passwords;
var webSQL;

chrome.storage.sync.get({

  appcache: false,
  cache: false,
  cookies: false,
  downloads: false,
  fileSystems: false,
  formData: false,
  chistory: false,
  cindexedDB: false,
  clocalStorage: false,
  serverBoundCertificates: false,
  pluginData: false,
  passwords: false,
  webSQL: false

}, function(items) {

  appcache = items.appcache;
  cache = items.cache;
  cookies = items.cookies;
  downloads = items.downloads;
  fileSystems = items.fileSystems;
  formData = items.formData;
  chistory = items.chistory;
  cindexedDB = items.cindexedDB;
  clocalStorage = items.clocalStorage;
  serverBoundCertificates = items.serverBoundCertificates;
  pluginData = items.pluginData;
  passwords = items.passwords;
  webSQL = items.webSQL;

  window.onbeforeunload = chrome.browsingData.remove({
      "originTypes": {
          "unprotectedWeb": true
      }
  }, {
      "appcache": appcache,
      "cache": cache,
      "cookies": cookies,
      "downloads": downloads,
      "fileSystems": fileSystems,
      "formData": formData,
      "history": chistory,
      "indexedDB": cindexedDB,
      "localStorage": clocalStorage,
      "serverBoundCertificates": serverBoundCertificates,
      "pluginData": pluginData,
      "passwords": passwords,
      "webSQL": webSQL
  }, onHistoryWipe);

});




function onHistoryWipe() {

}
