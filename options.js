

function save_options() {

var appcache = document.getElementById('appcache').checked;
var cache = document.getElementById('cache').checked;
var cookies = document.getElementById('cookies').checked;
var downloads = document.getElementById('downloads').checked;
var fileSystems = document.getElementById('fileSystems').checked;
var formData = document.getElementById('formData').checked;
var chistory = document.getElementById('chistory').checked;
var cindexedDB = document.getElementById('cindexedDB').checked;
var clocalStorage = document.getElementById('clocalStorage').checked;
var serverBoundCertificates = document.getElementById('serverBoundCertificates').checked;
var pluginData = document.getElementById('pluginData').checked;
var passwords = document.getElementById('passwords').checked;
var webSQL = document.getElementById('webSQL').checked;


  chrome.storage.sync.set({

    appcache: appcache,
    cache: cache,
    cookies: cookies,
    downloads: downloads,
    fileSystems: fileSystems,
    formData: formData,
    chistory: chistory,
    cindexedDB: cindexedDB,
    clocalStorage: clocalStorage,
    serverBoundCertificates: serverBoundCertificates,
    pluginData: pluginData,
    passwords: passwords,
    webSQL: webSQL

  }, function() {


    var status = document.getElementById('status');
    status.textContent = 'Options saved.';

    setTimeout(function()
    {
      status.textContent = '';
    }, 750);

  });
}


function restore_options() {

  chrome.storage.sync.get({

    appcache: false,
    cache: false,
    cookies: false,
    downloads: false,
    fileSystems: false,
    formData: false,
    chistory: false,
    cindexedDB: false,
    clocalStorage: false,
    serverBoundCertificates: false,
    pluginData: false,
    passwords: false,
    webSQL: false

  }, function(items) {

    document.getElementById('appcache').checked = items.appcache;
    document.getElementById('cache').checked = items.cache;
    document.getElementById('cookies').checked = items.cookies;
    document.getElementById('downloads').checked = items.downloads;
    document.getElementById('fileSystems').checked = items.fileSystems;
    document.getElementById('formData').checked = items.formData;
    document.getElementById('chistory').checked = items.chistory;
    document.getElementById('cindexedDB').checked = items.cindexedDB;
    document.getElementById('clocalStorage').checked = items.clocalStorage;
    document.getElementById('serverBoundCertificates').checked = items.serverBoundCertificates;
    document.getElementById('pluginData').checked = items.pluginData;
    document.getElementById('passwords').checked = items.passwords;
    document.getElementById('webSQL').checked = items.webSQL;

  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);
